import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {
  network: string;
  networks: string[] = ['bbc', 'cnn', 'nbc'];

  constructor(private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.network = this.route.snapshot.params.network;
    
  }
  

}
