// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDfnfpCEqkRDHVes4jY9okLBPhw2U8XAmw",
    authDomain: "hello-52ffb.firebaseapp.com",
    databaseURL: "https://hello-52ffb.firebaseio.com",
    projectId: "hello-52ffb",
    storageBucket: "hello-52ffb.appspot.com",
    messagingSenderId: "571965615539",
    appId: "1:571965615539:web:e7418e1c6acba6d6936fc6"}
  };


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
